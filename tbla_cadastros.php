<?php

$app->get("/cadastros", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM cadastros ORDER BY nome ASC";
	$stmt = DB::prepare($sql);

	$stmt->execute();
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson($nenhumRegistro, 404);
	}

	exit();
});

$app->get("/cadastros/{id}", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM cadastros WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':id' => $args['id']
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetch();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson($nenhumRegistro, 404);
	}

	exit();
});

$app->post("/cadastros", function ($request, $response, $args) {

	$data = $request->getParsedBody();

	$sql = "INSERT INTO cadastros (nome) VALUES (:nome)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':nome' => $data['nome']
	);

	$stmt->execute($colParams);

	echo json_encode(array("retorno" => true));
	exit();

});

$app->put("/cadastros/{id}", function ($request, $response, $args) {

	$data = $request->getParsedBody();

	$sql = "UPDATE cadastros SET nome = :nome WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':nome' => $data['nome'],
		':id' => $args['id']
	);

	$stmt->execute($colParams);

	echo json_encode(array("retorno" => true));
	exit();

});

$app->delete("/cadastros/{id}", function ($request, $response, $args) {

	$data = $request->getParsedBody();

	$sql = "DELETE FROM cadastros WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':id' => $args['id']
	);

	$stmt->execute($colParams);

	echo json_encode(array("retorno" => true));
	exit();

});

?>