<?php
session_start();
ini_set('safe_mode', '0');
ini_set('memory_limit', '-1');
set_time_limit(0);

@date_default_timezone_set('America/Sao_Paulo');
setlocale(LC_ALL, 'pt_BR');

header("Content-Type: application/json");

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "root");
define("DB_NAME", "db_padrao");

// @header("Access-Control-Allow-Origin: *");
// @header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH, OPTIONS");
// @header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization, token");
// @header("Access-Control-Max-Age: 86400");

include_once 'db.php';
include_once 'vendor/autoload.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$app = new Slim\App(["settings" => $config]);

$app->get('/', function ($request, $response, $args) {
	$response->write("Serviço Online!");
	return $response;
});

$nenhumRegistro = ['error' => 'Nenhum registro encontrado', 'code' => 404];

// Funções diversas
include_once "tbla_funcoes.php";
include_once "tbla_cadastros.php";
include_once "tbla_produtos.php";
include_once "tbla_pedidos.php";

/* CORS */
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, token')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
            ->withHeader('Access-Control-Request-Headers', 'token');
});

/* INTERCEPTOR */
$app->add(function ($request, $response, $next) {
    if(strpos($request->getUri()->getPath(), 'admin/logar') > 0) {
        if (isBlank(validarSessao())) {
            return $response->withJson([
                'error' => 'A sessao expirou!',
                'code' => 403
            ], 403);
        }
    }

    return $next($request, $response);
});

$app->run();

?>
