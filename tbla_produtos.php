<?php

$app->get("/produtos", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM produtos ORDER BY titulo ASC";
	$stmt = DB::prepare($sql);

	$stmt->execute();
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson($nenhumRegistro, 404);
	}

	exit();
});

$app->get("/produtos/{id}", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM produtos WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':id' => $args['id']
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetch();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson($nenhumRegistro, 404);
	}

	exit();
});

$app->post("/produtos", function ($request, $response, $args) {

	$data = $request->getParsedBody();

	$sql = "INSERT INTO produtos (titulo, descricao, valor) VALUES (:titulo, :descricao, :valor)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':titulo' => $data['titulo'],
		':descricao' => $data['descricao'],
		':valor' => $data['valor']
	);

	$stmt->execute($colParams);

	echo json_encode(array("retorno" => true));
	exit();

});

$app->put("/produtos/{id}", function ($request, $response, $args) {

	$data = $request->getParsedBody();

	$sql = "UPDATE produtos SET titulo = :titulo, descricao = :descricao, valor = :valor WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':titulo' => $data['titulo'],
		':descricao' => $data['descricao'],
		':valor' => $data['valor'],
		':id' => $args['id']
	);

	$stmt->execute($colParams);

	echo json_encode(array("retorno" => true));
	exit();

});

$app->delete("/produtos/{id}", function ($request, $response, $args) {

	$data = $request->getParsedBody();

	$sql = "DELETE FROM produtos WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':id' => $args['id']
	);

	$stmt->execute($colParams);

	echo json_encode(array("retorno" => true));
	exit();

});

?>