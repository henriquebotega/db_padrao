<?php

$app->get("/pedidos", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "
		SELECT c.*, cad.nome
		
		FROM pedidos c
		INNER JOIN cadastros cad ON cad.id = c.id_cadastro
		
		ORDER BY c.id DESC
	";

	$stmt = DB::prepare($sql);

	$stmt->execute();
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson($nenhumRegistro, 404);
	}

	exit();
});

$app->get("/pedidos/{id}", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "
		SELECT c.*, cad.nome
		
		FROM pedidos c
		INNER JOIN cadastros cad ON cad.id = c.id_cadastro
		
		WHERE (c.id = :id)
		ORDER BY c.id DESC
	";

	$stmt = DB::prepare($sql);

	$colParams = array(
		':id' => $args['id']
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetch();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson($nenhumRegistro, 404);
	}

	exit();
});

$app->get("/pedidos/{id_pedido}/itens", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "
		SELECT ic.*, p.titulo as 'produto'
		
        FROM itens_pedido ic
		INNER JOIN produtos p ON p.id = ic.id_produto
		
		WHERE (ic.id_pedido = :id_pedido)
	";

	$stmt = DB::prepare($sql);

	$colParams = array(
		':id_pedido' => $args['id_pedido']
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson($nenhumRegistro, 404);
	}

	exit();
});

?>