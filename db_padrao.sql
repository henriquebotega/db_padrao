-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 03/06/2019 às 13:17
-- Versão do servidor: 5.7.26-0ubuntu0.19.04.1
-- Versão do PHP: 7.2.17-0ubuntu0.19.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `db_padrao`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin`
--

CREATE TABLE `admin` (
  `id` int(15) NOT NULL,
  `login` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `data_ultimo_acesso` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `id_tipo` int(15) NOT NULL DEFAULT '0' COMMENT '0 = usuário, 1 = admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `admin`
--

INSERT INTO `admin` (`id`, `login`, `senha`, `data_ultimo_acesso`, `email`, `id_tipo`) VALUES
(1, 'teste', 'teste', '2019-06-03 12:54:40', 'teste@servidor.com', 1),
(5, 'asd', 'asd', '2019-06-03 13:15:03', 'asd@servidor.com', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cadastros`
--

CREATE TABLE `cadastros` (
  `id` int(15) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `cadastros`
--

INSERT INTO `cadastros` (`id`, `nome`) VALUES
(1, 'anderson');

-- --------------------------------------------------------

--
-- Estrutura para tabela `itens_pedido`
--

CREATE TABLE `itens_pedido` (
  `id` int(15) NOT NULL,
  `quantidade` int(15) NOT NULL,
  `id_produto` int(15) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `id_pedido` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `itens_pedido`
--

INSERT INTO `itens_pedido` (`id`, `quantidade`, `id_produto`, `valor`, `id_pedido`) VALUES
(1, 2, 1, '222.22', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(15) NOT NULL,
  `id_cadastro` int(15) NOT NULL,
  `data` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `pedidos`
--

INSERT INTO `pedidos` (`id`, `id_cadastro`, `data`) VALUES
(1, 1, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(15) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `valor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `produtos`
--

INSERT INTO `produtos` (`id`, `titulo`, `descricao`, `valor`) VALUES
(1, 'tv', 'televisao grande', '1500.36');

-- --------------------------------------------------------

--
-- Estrutura para tabela `sessao`
--

CREATE TABLE `sessao` (
  `id` int(15) NOT NULL,
  `codigo` varchar(255) NOT NULL,
  `data_limite` varchar(255) NOT NULL,
  `data_entrada` varchar(255) NOT NULL,
  `id_admin` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `sessao`
--

INSERT INTO `sessao` (`id`, `codigo`, `data_limite`, `data_entrada`, `id_admin`) VALUES
(1, '1', '1', '1', 1),
(2, 'b729ec6cb8efd04ac1da1524ad6fbc28', '2019-05-30 17:27:08', '2019-05-30 16:27:08', 1),
(3, '2b5b5fffc5ab93b9ce3c819bb6ac356e', '2019-05-30 17:27:16', '2019-05-30 16:27:16', 1),
(4, '795131f85cd38e860dd6b8bf7a293c62', '2019-05-30 17:28:58', '2019-05-30 16:28:58', 1),
(5, 'c989bbdf341b111e769f2ee359bd8df6', '2019-05-30 17:29:33', '2019-05-30 16:29:33', 1),
(6, '978f19561c5a6dfeb4dc406bd9062a08', '2019-05-30 17:41:26', '2019-05-30 16:41:26', 1),
(7, 'e15993b1a940ffd133629dcebc8d8a3b', '2019-05-30 17:41:31', '2019-05-30 16:41:31', 1),
(8, 'e8c1a0998111619cb7e326bab0fdae50', '2019-05-30 17:41:54', '2019-05-30 16:41:54', 1),
(9, '981f79f0fc303af34f6641de429431b5', '2019-05-30 17:42:27', '2019-05-30 16:42:27', 1),
(10, 'acf6fce51e36b51afee14934f7fdb7cf', '2019-05-30 17:43:29', '2019-05-30 16:43:29', 1),
(11, '02d778862618015f08b9f26e3f03d2af', '2019-05-30 17:52:27', '2019-05-30 16:52:27', 1),
(12, '08681282430ee2dfe880d188ce7289c2', '2019-05-31 08:54:49', '2019-05-31 07:54:49', 1),
(13, '303e60d0b4f88460374db55b62ea1274', '2019-05-31 10:23:11', '2019-05-31 09:23:11', 1),
(14, '56a370f5dbaf28b6c9bcbb97d7a5f03d', '2019-05-31 10:26:25', '2019-05-31 09:26:25', 1),
(15, '59b154278ea0acfadd857cfe23fa2f7c', '2019-05-31 10:29:02', '2019-05-31 09:29:02', 1),
(16, '591221c97b5c76f88113dddab5622b10', '2019-05-31 10:29:23', '2019-05-31 09:29:23', 1),
(17, 'b1c3161f3686be0927dd2835b744a0a4', '2019-05-31 10:30:00', '2019-05-31 09:30:00', 1),
(18, 'b22d29339383a3aaf528a12e3aaa1f0c', '2019-05-31 10:31:55', '2019-05-31 09:31:55', 1),
(19, 'd714c856ac86c21d5e5fa378d9ecb2ba', '2019-05-31 10:34:21', '2019-05-31 09:34:21', 1),
(20, '97363daf65a92b2fba126f5a647a1157', '2019-05-31 10:36:13', '2019-05-31 09:36:13', 1),
(21, 'a57405fddfef9195dae0e18c3eb4f06e', '2019-05-31 10:39:35', '2019-05-31 09:39:35', 1),
(22, '7554bf31ec290418812bb5454d5c63ca', '2019-05-31 10:42:19', '2019-05-31 09:42:19', 1),
(23, '49e3a31bd0cf008eb078183611885bbb', '2019-05-31 10:43:35', '2019-05-31 09:43:35', 1),
(24, 'd52afa914d957377c21c6091f43e4ea5', '2019-05-31 10:44:57', '2019-05-31 09:44:57', 1),
(25, '16b16c96adcd2e5bb3f4beb3e30ddf22', '2019-05-31 10:51:58', '2019-05-31 09:51:58', 1),
(26, '78f03a7633c6b60395c29981e8fdc7a5', '2019-05-31 10:52:49', '2019-05-31 09:52:49', 1),
(27, 'f0e056652f05b206b0f402821142be51', '2019-05-31 10:53:05', '2019-05-31 09:53:05', 1),
(28, '80ac076c6aa236c9ccfc368482fa551d', '2019-05-31 10:54:04', '2019-05-31 09:54:04', 1),
(29, '661395c5b88d8de1fed2864d95cba29c', '2019-05-31 10:55:01', '2019-05-31 09:55:01', 1),
(30, '615db293c5814278a7bc8d06fae33ac6', '2019-05-31 10:55:30', '2019-05-31 09:55:30', 1),
(31, 'a4c475afe20bb8a46644fcffd74253e1', '2019-05-31 11:48:48', '2019-05-31 10:48:48', 1),
(32, '032941818818fb1319db805bba4aa4a9', '2019-05-31 11:48:49', '2019-05-31 10:48:49', 1),
(33, '614c5b28714e5cdc899f52d98c86f1fc', '2019-05-31 11:48:49', '2019-05-31 10:48:49', 1),
(34, '7a768c41331de2660048d96dc0ebef08', '2019-05-31 11:48:57', '2019-05-31 10:48:57', 1),
(35, 'f2a4f57867c017b375883505d30c558b', '2019-05-31 11:48:57', '2019-05-31 10:48:57', 1),
(36, '6337c6de97acce77dce46753f8a8852d', '2019-05-31 11:48:57', '2019-05-31 10:48:57', 1),
(37, '90dc58b239b2389d8013c4ea32842c79', '2019-05-31 11:50:20', '2019-05-31 10:50:20', 1),
(38, 'b6d2987f2acf114d68d14a389bdbb837', '2019-05-31 11:50:21', '2019-05-31 10:50:21', 1),
(39, 'c54801bd222f8c723fcf70f10204f9fe', '2019-05-31 11:50:21', '2019-05-31 10:50:21', 1),
(40, '3ad343579627efb470a1af607b390126', '2019-05-31 11:50:38', '2019-05-31 10:50:38', 1),
(41, '8d87ac7e0a56b9e3aac59319ed848078', '2019-05-31 11:50:38', '2019-05-31 10:50:38', 1),
(42, 'aaae43dc3ab3af1dd576f9b55e560643', '2019-05-31 11:50:38', '2019-05-31 10:50:38', 1),
(43, '4eb730aaf031fd998399505019f05cd7', '2019-05-31 11:51:34', '2019-05-31 10:51:34', 1),
(44, 'cc5a5a71f3c564ec3ea09359f41aad68', '2019-05-31 11:52:40', '2019-05-31 10:52:40', 1),
(45, 'ebe3b7e83321e0b750f6be7f5958fcd8', '2019-05-31 11:53:17', '2019-05-31 10:53:17', 1),
(46, '0eace0b2d2f2cee0a14ba7d428ca2339', '2019-05-31 11:53:43', '2019-05-31 10:53:43', 1),
(47, '9629b7f0dd1a1a9154361314c73ec2d9', '2019-05-31 11:55:00', '2019-05-31 10:55:00', 1),
(48, '86034177c4d9ef11b0cd9b94bd69217e', '2019-05-31 11:55:19', '2019-05-31 10:55:19', 1),
(49, 'a8000732b48f54e34e0ad16e7e2b474a', '2019-05-31 11:55:50', '2019-05-31 10:55:50', 1),
(50, '61b08f207645545231ce4ed60b5fe410', '2019-05-31 11:56:14', '2019-05-31 10:56:14', 1),
(51, 'ec11bf2ee9d068a9edc11fa37d9c53bc', '2019-05-31 12:11:31', '2019-05-31 11:11:31', 1),
(52, 'cfb319dc5d313a391a2f4de507b33fa0', '2019-05-31 12:13:01', '2019-05-31 11:13:01', 1),
(53, '79f8b0a59e67f1cf79e8c4f32d081270', '2019-05-31 12:13:01', '2019-05-31 11:13:01', 1),
(54, '4b12fcd5a015b9576f8a6e88e37a3e89', '2019-05-31 12:13:59', '2019-05-31 11:13:59', 1),
(55, 'a116ed1510c0e9d41c05d80fd6c0df32', '2019-05-31 12:14:13', '2019-05-31 11:14:13', 1),
(56, '9d24a0320d4cb2d99c4a75abc1ab49d0', '2019-05-31 12:14:33', '2019-05-31 11:14:33', 1),
(57, 'f0ff0d3b4b33f2e2d64bbecddebe549e', '2019-05-31 12:14:33', '2019-05-31 11:14:33', 1),
(58, 'f165eb65a0a3d10515bea6f7159ab99e', '2019-05-31 12:14:38', '2019-05-31 11:14:38', 1),
(59, '87c0e9127e91749e0395310a593fbc06', '2019-05-31 12:14:38', '2019-05-31 11:14:38', 1),
(60, '90598d7aba3b8547bdf3cb2af968d05f', '2019-05-31 12:15:16', '2019-05-31 11:15:16', 1),
(61, '2abfe77ac7a3f2064d0f64ca4ec77566', '2019-05-31 12:15:21', '2019-05-31 11:15:21', 1),
(62, 'd858afcaeffef333539e4406c3ab1902', '2019-05-31 12:18:45', '2019-05-31 11:18:45', 1),
(63, '34de0c31625f78e82836065b1dfadcb6', '2019-05-31 12:19:15', '2019-05-31 11:19:15', 1),
(64, 'eba45fb07b472bac64eb21627165858e', '2019-05-31 12:20:39', '2019-05-31 11:20:39', 1),
(65, 'b05e6843eaebe266ccf3de63af90e614', '2019-05-31 14:15:53', '2019-05-31 13:15:53', 1),
(66, '4171dc73bd02c018e28e105985429614', '2019-05-31 15:19:07', '2019-05-31 14:19:07', 1),
(67, '42d4e3d424e3eee4c32a35dda51eec45', '2019-05-31 15:19:33', '2019-05-31 14:19:33', 1),
(68, '70a33a4ec92e7540f955d6ba231b8b0b', '2019-05-31 15:25:45', '2019-05-31 14:25:45', 1),
(69, '0d40167652a28e3bb7aef3b68fb37af7', '2019-05-31 15:29:41', '2019-05-31 14:29:41', 1),
(70, '73e5ab511a0a09375c75e884a4d76b01', '2019-05-31 15:30:03', '2019-05-31 14:30:03', 1),
(71, '16362f3922851fda8dcbe52171c6a078', '2019-05-31 15:34:40', '2019-05-31 14:34:40', 1),
(72, '51ef04156504ab6e5eb5d69a5015a62e', '2019-05-31 16:15:18', '2019-05-31 15:15:18', 1),
(73, '76866e00de020450702a3f98274ae889', '2019-06-03 09:13:26', '2019-06-03 08:13:26', 1),
(74, '4baf8a5da4ead707946692cd3845d8d5', '2019-06-03 13:31:18', '2019-06-03 12:31:18', 1),
(75, '64a78d0dab829b3771693af8188c4350', '2019-06-03 13:31:29', '2019-06-03 12:31:29', 1),
(76, '513abd542f1e016859ed7afa65bd08c9', '2019-06-03 13:44:19', '2019-06-03 12:44:19', 1),
(77, '3fd54a097b107543349b2328fd14936f', '2019-06-03 13:54:40', '2019-06-03 12:54:40', 1),
(78, '854c33683c5118c78e18dd5b308ba6fc', '2019-06-03 14:15:03', '2019-06-03 13:15:03', 5);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `cadastros`
--
ALTER TABLE `cadastros`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `itens_pedido`
--
ALTER TABLE `itens_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `sessao`
--
ALTER TABLE `sessao`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `cadastros`
--
ALTER TABLE `cadastros`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `itens_pedido`
--
ALTER TABLE `itens_pedido`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `sessao`
--
ALTER TABLE `sessao`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
